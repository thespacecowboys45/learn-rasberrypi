#!/usr/bin/python
import board
import time
from PIL import Image

from adafruit_ht16k33.matrix import Matrix8x8

print("Blink")

i2c = board.I2C()
matrix = Matrix8x8(i2c)


while True:
    matrix.fill(1)
    time.sleep(1)
    matrix.fill(0)
    time.sleep(1)

    image = Image.open("led_matrices_squares-mono-8x8.png")
    matrix.image(image)

    time.sleep(4)
    matrix.fill(0)
