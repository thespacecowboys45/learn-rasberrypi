#!/usr/bin/python
import board
import time

from adafruit_ht16k33.matrix import Matrix8x8

print("Blink")

i2c = board.I2C()
matrix = Matrix8x8(i2c)


while True:
    matrix.fill(1)
    time.sleep(1)
    matrix.fill(0)
    time.sleep(1)
