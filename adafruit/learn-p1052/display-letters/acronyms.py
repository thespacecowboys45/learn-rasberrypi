import board
import time
from adafruit_ht16k33.matrix import Matrix8x8

import globals as g
import letters as l

def uhtbi(pause):
    l.U(pause)
    l.H(pause)
    l.T(pause)
    l.B(pause)
    l.I(pause)
