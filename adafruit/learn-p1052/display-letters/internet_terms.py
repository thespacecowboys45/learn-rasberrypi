import board
import time
from adafruit_ht16k33.matrix import Matrix8x8

import globals as g
import letters as l
import symbols as s

def dotcom(pause):
    s.period(pause)
    l.C(pause)
    l.O(pause)
    l.M(pause)
    
