import board
import time
from adafruit_ht16k33.matrix import Matrix8x8

import globals as g
import letters as l
import symbols as s

def hi(pause):
    l.H(pause)
    l.I(pause)

def dad(pause):
    l.D(pause)
    l.A(pause)
    l.D(pause)


def mom(pause):
    l.M(pause)
    l.O(pause)
    l.M(pause)

def booty(pause):
    l.B(pause)
    l.O(pause)
    l.O(pause)
    l.T(pause)
    l.Y(pause)

def the(pause):
    l.T(pause)
    l.H(pause)
    l.E(pause)

def youre(pause):
    l.Y(pause)
    l.O(pause)
    l.U(pause)
    s.apostrophe(pause)
    l.R(pause)
    l.E(pause)

def youve(pause):
    l.Y(pause)
    l.O(pause)
    l.U(pause)
    s.apostrophe(pause)
    l.V(pause)
    l.E(pause)

def best(pause):
    l.B(pause)
    l.E(pause)
    l.S(pause)
    l.T(pause)

def pressed(pause):
    l.P(pause)
    l.R(pause)
    l.E(pause)
    l.S(pause)
    l.S(pause)
    l.E(pause)
    l.D(pause)

def times(pause):
    l.T(pause)
    l.I(pause)
    l.M(pause)
    l.E(pause)
    l.S(pause)

