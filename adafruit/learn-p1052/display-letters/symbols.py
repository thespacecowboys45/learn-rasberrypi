import board
import time
from adafruit_ht16k33.matrix import Matrix8x8

import globals as g

def createMatrix(bitmask):
    print("createMatrix()")
    nrow = len(bitmask)
    for i in range(nrow): 
#        print ("i=",i)
        row=bitmask[i]
        ncol=len(row)
        for j in range(ncol):
#            print("j=",j)
            val=row[j]
            #print("[",i,",",j,"]=",val)
            # NOTE: the matrix runs col,row
            # Whereas our natural human brain
            # runs rol, col
            g.matrix[j,i]=val



'''

  01234567
0 --------
1 -++++++-
2 -+------
3 -+------
4 -++++++-
5 -+------
6 -++++++-
7 --------

'''
def period(pause):
    print ("display period")
    #           0 1 2 3 4 5 6 7
    bitmask = [[0,0,0,0,0,0,0,0], # 0
               [0,0,0,0,0,0,0,0], # 1
               [0,0,0,0,0,0,0,0], # 2
               [0,0,0,0,0,0,0,0], # 3
               [0,0,0,0,0,0,0,0], # 4
               [0,0,0,1,1,0,0,0], # 5
               [0,0,0,1,1,0,0,0], # 6
               [0,0,0,0,0,0,0,0]] # 7

    createMatrix(bitmask)
    if pause:
        g.letterpause()

'''
    Bitmask shows pattern
'''
def apostrophe(pause):
    print ("display period")
    #           0 1 2 3 4 5 6 7
    bitmask = [[0,0,0,0,0,0,0,0], # 0
               [0,0,0,1,1,0,0,0], # 1
               [0,0,1,1,1,1,0,0], # 2
               [0,0,0,0,1,0,0,0], # 3
               [0,0,0,1,0,0,0,0], # 4
               [0,0,0,0,0,0,0,0], # 5
               [0,0,0,0,0,0,0,0], # 6
               [0,0,0,0,0,0,0,0]] # 7

    createMatrix(bitmask)
    if pause:
        g.letterpause()

'''
    Bitmask shows pattern

'''
def dotdotdot(pause):
    print ("display dotdotdot")
    #           0 1 2 3 4 5 6 7
    bitmask = [[0,0,0,0,0,0,0,0], # 0
               [0,0,0,0,0,0,0,0], # 1
               [0,0,0,0,0,0,0,0], # 2
               [0,0,0,0,0,0,0,0], # 3
               [0,0,0,0,0,0,0,0], # 4
               [1,1,0,1,1,0,1,1], # 5
               [1,1,0,1,1,0,1,1], # 6
               [0,0,0,0,0,0,0,0]] # 7

    createMatrix(bitmask)
    if pause:
        g.letterpause()

