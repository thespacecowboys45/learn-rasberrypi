import board
import time
from adafruit_ht16k33.matrix import Matrix8x8

i2c = board.I2C()
matrix = Matrix8x8(i2c)

LETTER_PAUSETIME=0.5
WORD_PAUSETIME=2*LETTER_PAUSETIME

def set_letterpause(duration):
    global LETTER_PAUSETIME
    global WORD_PAUSETIME
    LETTER_PAUSETIME=duration
    WORD_PAUSETIME=2*LETTER_PAUSETIME

def letterpause():
    print("letterpause - " , LETTER_PAUSETIME)
    time.sleep(LETTER_PAUSETIME)
    clear()

def wordpause():
    time.sleep(WORD_PAUSETIME)

def clear():
    matrix.fill(0)
