#!/bin/bash
#
# @date 2020.07.19
# @author Space Cowboy
#
# DESCRIPTION:
#
#   Install the blink python program as a service
#
# NOTE:
#
#   Run this as root, or with sudo priv
#
######
mkdir -p /opt/display-controller
cp *.py /opt/display-controller
cp display-controller.service /etc/systemd/system/
systemctl enable display-controller
systemctl daemon-reload
