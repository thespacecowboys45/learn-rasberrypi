import board
import time
from adafruit_ht16k33.matrix import Matrix8x8

import globals as g

def createMatrix(bitmask):
    print("createMatrix()")
    nrow = len(bitmask)
    for i in range(nrow): 
#        print ("i=",i)
        row=bitmask[i]
        ncol=len(row)
        for j in range(ncol):
#            print("j=",j)
            val=row[j]
            #print("[",i,",",j,"]=",val)
            # NOTE: the matrix runs col,row
            # Whereas our natural human brain
            # runs rol, col
            g.matrix[j,i]=val



'''

  01234567
0 --------
1 -++++++-
2 -+------
3 -+------
4 -++++++-
5 -+------
6 -++++++-
7 --------

'''
def one(pause):
    print ("display 1")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,1,0,1,0,0,0],
               [0,0,0,0,1,0,0,0],
               [0,0,0,0,1,0,0,0],
               [0,0,0,0,1,0,0,0],
               [0,0,1,1,1,1,1,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def two(pause):
    print ("display 2")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,1,0,0,1,0,0],
               [0,0,0,0,0,1,0,0],
               [0,0,0,0,1,0,0,0],
               [0,0,0,1,0,0,0,0],
               [0,0,1,1,1,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()


'''

 Bitmask shows pattern

'''
def three(pause):
    print ("display 3")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,1,1,1,1,0,0],
               [0,0,0,0,0,1,0,0],
               [0,0,0,1,1,1,0,0],
               [0,0,0,0,0,1,0,0],
               [0,0,0,0,0,1,0,0],
               [0,0,1,1,1,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()


'''

 Bitmask shows pattern

'''
def four(pause):
    print ("display 4")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,1,0,1,0,0,0],
               [0,0,1,0,1,0,0,0],
               [0,0,1,1,1,1,0,0],
               [0,0,0,0,1,0,0,0],
               [0,0,0,0,1,0,0,0],
               [0,0,0,0,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()


'''

 Bitmask shows pattern

'''
def five(pause):
    print ("display 5")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,1,1,1,1,1,0],
               [0,0,1,0,0,0,0,0],
               [0,0,1,1,1,1,0,0],
               [0,0,0,0,0,0,1,0],
               [0,0,1,0,0,0,1,0],
               [0,0,0,1,1,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()

'''

 Bitmask shows pattern

'''
def six(pause):
    print ("display 6")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,0,1,1,1,0,0],
               [0,0,1,0,0,0,0,0],
               [0,0,1,1,1,1,0,0],
               [0,0,1,0,0,0,1,0],
               [0,0,1,0,0,0,1,0],
               [0,0,0,1,1,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()

'''

 Bitmask shows pattern

'''
def seven(pause):
    print ("display 7")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,1,1,1,1,1,0],
               [0,0,0,0,0,1,1,0],
               [0,0,0,0,1,1,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()

'''

 Bitmask shows pattern

'''
def eight(pause):
    print ("display 8")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,1,0,0,1,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,1,0,0,1,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()

'''

 Bitmask shows pattern

'''
def nine(pause):
    print ("display 9")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,0,1,1,1,0,0],
               [0,0,1,0,0,0,1,0],
               [0,0,0,1,1,1,1,0],
               [0,0,0,0,0,1,1,0],
               [0,0,0,0,1,1,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()

'''

 Bitmask shows pattern

'''
def zero(pause):
    print ("display 0")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,0,1,1,1,0,0],
               [0,0,1,0,0,0,1,0],
               [0,0,1,0,1,0,1,0],
               [0,0,1,0,1,0,1,0],
               [0,0,1,0,0,0,1,0],
               [0,0,0,1,1,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()

