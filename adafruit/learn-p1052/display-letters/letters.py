import board
import time
from adafruit_ht16k33.matrix import Matrix8x8

import globals as g

def createMatrix(bitmask):
    print("createMatrix()")
    nrow = len(bitmask)
    for i in range(nrow): 
#        print ("i=",i)
        row=bitmask[i]
        ncol=len(row)
        for j in range(ncol):
#            print("j=",j)
            val=row[j]
            #print("[",i,",",j,"]=",val)
            # NOTE: the matrix runs col,row
            # Whereas our natural human brain
            # runs rol, col
            g.matrix[j,i]=val



'''

  01234567
0 --------
1 ---++---
2 --+--+--
3 -+----+-
4 -++++++-
5 -+----+-
6 -+----+-
7 --------

'''
def A(pause):
    print ("display A")
    g.matrix[1,3]=1
    g.matrix[1,4]=1
    g.matrix[1,5]=1
    g.matrix[1,6]=1
    g.matrix[2,2]=1
    g.matrix[2,4]=1
    g.matrix[3,1]=1
    g.matrix[3,4]=1
    g.matrix[4,1]=1
    g.matrix[4,4]=1
    g.matrix[5,2]=1
    g.matrix[5,4]=1
    g.matrix[6,3]=1
    g.matrix[6,4]=1
    g.matrix[6,5]=1
    g.matrix[6,6]=1
    if pause:
        g.letterpause()

'''

  01234567
0 --------
1 -+++++--
2 -+----+-
3 -+++++--
4 -+++++--
5 -+----+-
6 -+++++--
7 --------

'''
def B(pause):
    print ("display B")
    g.matrix[1,1]=1
    g.matrix[1,2]=1
    g.matrix[1,3]=1
    g.matrix[1,4]=1
    g.matrix[1,5]=1
    g.matrix[1,6]=1
    g.matrix[2,4]=1
    g.matrix[3,4]=1
    g.matrix[4,4]=1
    g.matrix[5,4]=1
    g.matrix[2,3]=1
    g.matrix[3,3]=1
    g.matrix[4,3]=1
    g.matrix[5,3]=1
    g.matrix[2,1]=1
    g.matrix[3,1]=1
    g.matrix[4,1]=1
    g.matrix[5,1]=1
    g.matrix[2,6]=1
    g.matrix[3,6]=1
    g.matrix[4,6]=1
    g.matrix[5,6]=1
    g.matrix[6,2]=1
    g.matrix[6,5]=1
    if pause:
        g.letterpause()



'''

  01234567
0 --------
1 --+++++-
2 -+------
3 -+------
4 -+------
5 -+------
6 --+++++-
7 --------

'''
def C(pause):
    print ("display C")
    g.matrix[1,2]=1
    g.matrix[1,3]=1
    g.matrix[1,4]=1
    g.matrix[1,5]=1
    g.matrix[2,6]=1
    g.matrix[3,6]=1
    g.matrix[4,6]=1
    g.matrix[5,6]=1
    g.matrix[6,6]=1

    g.matrix[2,1]=1
    g.matrix[3,1]=1
    g.matrix[4,1]=1
    g.matrix[5,1]=1
    g.matrix[6,1]=1
    if pause:
        g.letterpause()



'''

  01234567
0 --------
1 -+++++--
2 -+---++-
3 -+----+-
4 -+----+-
5 -+---++-
6 -+++++--
7 --------

'''
def D(pause):
    print ("display D")
    g.matrix[1,1]=1
    g.matrix[1,2]=1
    g.matrix[1,3]=1
    g.matrix[1,4]=1
    g.matrix[1,5]=1
    g.matrix[1,6]=1
    g.matrix[2,1]=1
    g.matrix[2,6]=1
    g.matrix[3,1]=1
    g.matrix[3,6]=1
    g.matrix[4,1]=1
    g.matrix[4,6]=1
    g.matrix[5,1]=1
    g.matrix[5,2]=1
    g.matrix[5,5]=1
    g.matrix[5,6]=1
    g.matrix[6,2]=1
    g.matrix[6,3]=1
    g.matrix[6,4]=1
    g.matrix[6,5]=1
    if pause:
        g.letterpause()


'''

  01234567
0 --------
1 -++++++-
2 -+------
3 -+------
4 -++++++-
5 -+------
6 -++++++-
7 --------

'''
def E(pause):
    print ("display E")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,1,1,1,1,1,0],
               [0,1,0,0,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,1,1,1,1,1,1,0],
               [0,1,0,0,0,0,0,0],
               [0,1,1,1,1,1,1,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def F(pause):
    print ("display F")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,1,1,1,1,1,0],
               [0,1,0,0,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,1,1,1,1,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()

'''

 Bitmask shows pattern

'''
def G(pause):
    print ("display G")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,1,0,0,1,0,0],
               [0,0,1,0,0,0,0,0],
               [0,0,1,0,1,1,0,0],
               [0,0,1,0,0,1,0,0],
               [0,0,0,1,1,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()
'''

  01234567
0 --------
1 -+----+-
2 -+----+-
3 -+----+-
4 -++++++-
5 -+----+-
6 -+----+-
7 --------

'''
def H(pause):
    print ("display H")
    g.matrix[1,1]=1
    g.matrix[1,2]=1
    g.matrix[1,3]=1
    g.matrix[1,4]=1
    g.matrix[1,5]=1
    g.matrix[1,6]=1
    g.matrix[2,4]=1
    g.matrix[3,4]=1
    g.matrix[4,4]=1
    g.matrix[5,4]=1
    g.matrix[6,1]=1
    g.matrix[6,2]=1
    g.matrix[6,3]=1
    g.matrix[6,4]=1
    g.matrix[6,5]=1
    g.matrix[6,6]=1
    if pause:
        g.letterpause()




'''

  01234567
0 --------
1 -++++++-
2 ---++---
3 ---++---
4 ---++---
5 ---++---
6 -++++++-
7 --------

'''
def I(pause):
    print ("display I")
    g.matrix[1,1]=1
    g.matrix[2,1]=1
    g.matrix[3,1]=1
    g.matrix[4,1]=1
    g.matrix[5,1]=1
    g.matrix[6,1]=1
    g.matrix[3,2]=1
    g.matrix[3,3]=1
    g.matrix[3,4]=1
    g.matrix[3,5]=1
    g.matrix[3,6]=1
    g.matrix[4,2]=1
    g.matrix[4,3]=1
    g.matrix[4,4]=1
    g.matrix[4,5]=1
    g.matrix[4,6]=1

    g.matrix[1,6]=1
    g.matrix[2,6]=1
    g.matrix[5,6]=1
    g.matrix[6,6]=1

    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def J(pause):
    print ("display J")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,1,1,1,1,1,0],
               [0,0,0,0,1,0,0,0],
               [0,0,0,0,1,0,0,0],
               [0,0,1,0,1,0,0,0],
               [0,0,1,0,1,0,0,0],
               [0,0,1,1,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def K(pause):
    print ("display K")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,0,0,1,0,0,0],
               [0,1,0,1,0,0,0,0],
               [0,1,1,0,0,0,0,0],
               [0,1,1,0,0,0,0,0],
               [0,1,0,1,0,0,0,0],
               [0,1,0,0,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()
'''

 Bitmask shows pattern

'''
def L(pause):
    print ("display L")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,1,1,1,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()

'''

  01234567
0 --------
1 -++--++-
2 -+-++-+-
3 -+-++-+-
4 -+-++-+-
5 -+-++-+-
6 -+-++-+-
7 --------

'''
def M(pause):
    print ("display M")
    g.matrix[1,1]=1
    g.matrix[1,2]=1
    g.matrix[1,3]=1
    g.matrix[1,4]=1
    g.matrix[1,5]=1
    g.matrix[1,6]=1
    g.matrix[2,1]=1
    g.matrix[3,2]=1
    g.matrix[3,3]=1
    g.matrix[3,4]=1
    g.matrix[3,5]=1
    g.matrix[3,6]=1


    g.matrix[4,2]=1
    g.matrix[4,3]=1
    g.matrix[4,4]=1
    g.matrix[4,5]=1
    g.matrix[4,6]=1


    g.matrix[5,1]=1
    g.matrix[6,1]=1
    g.matrix[6,2]=1
    g.matrix[6,3]=1
    g.matrix[6,4]=1
    g.matrix[6,5]=1
    g.matrix[6,6]=1
    if pause:
        g.letterpause()


'''

 Bitmask shows pattern

'''
def N(pause):
    print ("display N")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,0,0,0,0,1,0],
               [0,1,1,0,0,0,1,0],
               [0,1,0,1,0,0,1,0],
               [0,1,0,0,1,0,1,0],
               [0,1,0,0,0,1,1,0],
               [0,1,0,0,0,0,1,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def O(pause):
    print ("display O")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,1,1,1,0,0,0],
               [0,1,0,0,0,1,0,0],
               [0,1,0,0,0,1,0,0],
               [0,1,0,0,0,1,0,0],
               [0,1,0,0,0,1,0,0],
               [0,0,1,1,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()



'''

  01234567
0 --------
1 --++++--
2 -+----+-
3 -+----+-
4 -+----+-
5 -+----+-
6 --++++--
7 --------

'''
def O_old(pause):
    print ("display O")
    g.matrix[1,2]=1
    g.matrix[1,3]=1
    g.matrix[1,4]=1
    g.matrix[1,5]=1
    g.matrix[2,6]=1
    g.matrix[3,6]=1
    g.matrix[4,6]=1
    g.matrix[5,6]=1
    g.matrix[6,5]=1
    g.matrix[6,4]=1
    g.matrix[6,3]=1
    g.matrix[6,2]=1

    g.matrix[2,1]=1
    g.matrix[3,1]=1
    g.matrix[4,1]=1
    g.matrix[5,1]=1
    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def P(pause):
    print ("display P")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,1,1,0,0,0,0],
               [0,1,0,0,1,0,0,0],
               [0,1,0,0,1,0,0,0],
               [0,1,1,1,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,1,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def Q(pause):
    print ("display Q")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,1,1,1,0,0,0],
               [0,1,0,0,0,1,0,0],
               [0,1,0,0,0,1,0,0],
               [0,1,0,1,0,1,0,0],
               [0,1,0,0,1,0,0,0],
               [0,0,1,1,0,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def R(pause):
    print ("display R")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,1,1,1,0,0,0],
               [0,1,0,0,0,1,0,0],
               [0,1,0,0,0,1,0,0],
               [0,1,1,1,1,0,0,0],
               [0,1,0,0,1,0,0,0],
               [0,1,0,0,0,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def S(pause):
    print ("display S")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,0,1,1,1,1,1,0],
               [0,1,0,0,0,0,0,0],
               [0,0,1,1,1,1,0,0],
               [0,0,0,0,0,0,1,0],
               [0,0,0,0,0,0,1,0],
               [0,1,1,1,1,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()


'''

  01234567
0 --------
1 -++++++-
2 ---++---
3 ---++---
4 ---++---
5 ---++---
6 ---++---
7 --------

'''
def T(pause):
    print ("display T")
    g.matrix[1,1]=1
    g.matrix[2,1]=1
    g.matrix[3,1]=1
    g.matrix[4,1]=1
    g.matrix[5,1]=1
    g.matrix[6,1]=1
    g.matrix[3,2]=1
    g.matrix[3,3]=1
    g.matrix[3,4]=1
    g.matrix[3,5]=1
    g.matrix[3,6]=1
    g.matrix[4,2]=1
    g.matrix[4,3]=1
    g.matrix[4,4]=1
    g.matrix[4,5]=1
    g.matrix[4,6]=1
    if pause:
        g.letterpause()



'''

  01234567
0 --------
1 -+----+-
2 -+----+-
3 -+----+-
4 -+----+-
5 -+----+-
6 --++++--
7 --------

'''
def U(pause):
    print ("display U")
    g.matrix[1,1]=1
    g.matrix[1,2]=1
    g.matrix[1,3]=1
    g.matrix[1,4]=1
    g.matrix[1,5]=1
    g.matrix[2,6]=1
    g.matrix[3,6]=1
    g.matrix[4,6]=1
    g.matrix[5,6]=1
    g.matrix[6,5]=1
    g.matrix[6,4]=1
    g.matrix[6,3]=1
    g.matrix[6,2]=1
    g.matrix[6,1]=1
    if pause:
        g.letterpause()
    

'''

 Bitmask shows pattern

'''
def V(pause):
    print ("display V")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,0,0,0,0,1,0],
               [0,1,0,0,0,0,1,0],
               [0,1,0,0,0,0,1,0],
               [0,1,0,0,0,0,1,0],
               [0,0,1,0,0,1,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()




'''

 Bitmask shows pattern

'''
def W(pause):
    print ("display W")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,0,1,1,0,1,0],
               [0,1,0,1,1,0,1,0],
               [0,1,0,1,1,0,1,0],
               [0,1,0,1,1,0,1,0],
               [0,1,0,1,1,0,1,0],
               [0,0,1,0,0,1,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()



'''

 Bitmask shows pattern

'''
def X(pause):
    print ("display X")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,0,0,0,0,1,0],
               [0,0,1,0,0,1,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,1,0,0,1,0,0],
               [0,1,0,0,0,0,1,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()





'''

 Bitmask shows pattern

'''
def Y(pause):
    print ("display Y")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,0,0,0,0,1,0],
               [0,0,1,0,0,1,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,1,1,0,0,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()




'''

 Bitmask shows pattern

'''
def Z(pause):
    print ("display Z")
    bitmask = [[0,0,0,0,0,0,0,0],
               [0,1,1,1,1,1,1,0],
               [0,0,0,0,0,1,0,0],
               [0,0,0,0,1,0,0,0],
               [0,0,0,1,0,0,0,0],
               [0,0,1,0,0,0,0,0],
               [0,1,1,1,1,1,1,0],
               [0,0,0,0,0,0,0,0]]

    createMatrix(bitmask)
    if pause:
        g.letterpause()


'''

  01234567
0 --------
1 --------
2 --------
3 --------
4 --------
5 ---++---
6 ---++---
7 --------

'''
def period(pause):
    print ("display Period")
    g.matrix[3,5]=1
    g.matrix[3,6]=1
    g.matrix[4,5]=1
    g.matrix[4,6]=1
    if pause:
        g.letterpause()

