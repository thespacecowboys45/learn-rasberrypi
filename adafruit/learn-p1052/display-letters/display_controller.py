'''
Python is dumb
'''
print("Hello world")

import board
import time
from adafruit_ht16k33.matrix import Matrix8x8

import globals as g
import letters as l
import words as w
import symbols as s
import numbers as n

def show_numbers():
    print("show_numbers()")
    n.one(1)
    n.two(1)
    n.three(1)
    n.four(1)
    n.five(1)
    n.six(1)
    n.seven(1)
    n.eight(1)
    n.nine(1)
    n.zero(1)

def show_letters():

    l.A(1)
    l.B(1)
    l.C(1)
    l.D(1)
    l.E(1)
    l.F(1)
    l.G(1)
    l.H(1)
    l.I(1)
    l.J(1)
    l.K(1)
    l.L(1)
    l.M(1)
    l.N(1)
    l.O(1)
    l.P(1)
    l.Q(1)
    l.R(1)
    l.S(1)
    l.T(1)
    l.U(1)
    l.V(1)
    l.W(1)
    l.X(1)
    l.Y(1)
    l.Z(1)

def show_words():
    w.youre(1)
    w.youve(1)

def show_symbols():
    s.period

g.clear()
show_numbers()
show_letters()
show_words()
show_symbols()
