P0152 adafruit
----------------
Wiring = RPi <--> CHIP

2 <--> VCC aka "+" (brown) 
3 <--> SDA aka "d" (orange)
5 <--> SCL aka "c" (yellow)
6 <--> GND aka "-" (red)

PYTHON CODE
-----------
https://learn.adafruit.com/adafruit-led-backpack/0-8-8x8-matrix-circuitpython-and-python-usage

RASPBERRY-PI
-------------
Must have I2C interface enabled.
Enable with 'sudo raspi-config'


NOTES
------
See if it detects the chip connected:

sudo i2cdetect -y 1



WEB LINKS
-----------
https://learn.adafruit.com/adafruit-gfx-graphics-library
https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-python-wiring-and-setup


LIBRARY DEPENDENCIES
-----------------------
https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-python-wiring-and-setup
pip3 install adafruit-circuitpython-ht16k33

sudo python3 -m pip install --force-reinstall adafruit-blinka
pip3 install adafruit-circuitpython-ht16k33
sudo apt-get install python3-pip
sudo apt-get install python3-pil


PRODUCT
--------
https://www.microcenter.com/product/456374/adafruit-industries-small-12-8x8-led-matrix-w-i2c-backpack---blue

https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-matrix
