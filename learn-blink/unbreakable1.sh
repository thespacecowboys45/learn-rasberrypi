#!/bin/bash

if [ "x$1" == "xdaemon" ]
then
	echo "Daemonize..."
	nohup python blink-w-button.py > /dev/null &
else
	echo "Run local"
	python blink-w-button.py
fi

