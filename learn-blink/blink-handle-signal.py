import signal
import sys
import time
import threading

def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
print('Press Ctrl+C to stop')
forever = threading.Event()
#forever.wait()



import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(4, GPIO.OUT)

# output a signal that the program is running
GPIO.output(4, True)


def handle_exception():
    print("handle_exception()")
    GPIO.output(4, False)
    GPIO.output(18, False)


def main():
    while True:
        print "On"
        GPIO.output(18, True)
        time.sleep(1)
        print "Off"
        GPIO.output(18, False)
        time.sleep(1)

if __name__ == "__main__":
    try:
        main()
    except:
        handle_exception()
