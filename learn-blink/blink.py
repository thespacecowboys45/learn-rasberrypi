import RPi.GPIO as GPIO
import time
import signal

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(4, GPIO.OUT)

# output a signal that the program is running
GPIO.output(4, True)

def handle_exception():
    print("handle_exception")
    GPIO.output(4, False)

def main():
    while True:
        print "On"
        GPIO.output(18, True)
        time.sleep(1)
        print "Off"
        GPIO.output(18, False)
        time.sleep(1)

# just to handle any exceptions
if __name__ == "__main__":
    try:
        main()
    except:
        handle_exception()
