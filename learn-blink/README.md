Useful links:

Blink with Go:

https://medium.com/@farissyariati/go-raspberry-pi-hello-world-tutorial-7e830d08b3ae


SETTING THIS UP AS A SERVICE TO RUN ON BOOT
--------------------------------------------
copy blink.service to /etc/systemd/system/blink.service

copy blink.py to /opt

systemctl enable blink

At this point, blink.py will run whenever the Pi boots.

To control:

systemctl start blink
systemctl stop blink
systemctl status blink
