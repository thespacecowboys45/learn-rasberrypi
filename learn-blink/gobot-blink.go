// NOTES:
// Physically wire this up just like the basic "blink" tutorial as found
// in the CanaKit version.  Basically, it's a 220 Ohm resistor wired to
// the cathode (shorter peg) of the LED, and then pin #7 (GPIO 4) wired
// to the anode (longer peg) of the LED.  Connect the power 3.3V to the
// + rail on the breadboard.  It makes no sense to me why you need this
// because it doesn't go anywhere really (the power), like it's not
// connected to anything.
//
// In fact, I unplugged the power and the LED kept blinking.  Not sure
// how that is, or why that is, just yet.
////////
package main

import (
        "time"

        "gobot.io/x/gobot"
        "gobot.io/x/gobot/drivers/gpio"
        "gobot.io/x/gobot/platforms/raspi"
)

func main() {
        r := raspi.NewAdaptor()
	led := gpio.NewLedDriver(r, "7")
	//led := gpio.NewLedDriver(r, "12")

        work := func() {
                gobot.Every(1*time.Second, func() {
                        led.Toggle()
                })
        }

        robot := gobot.NewRobot("blinkBot",
                []gobot.Connection{r},
                []gobot.Device{led},
                work,
        )

        robot.Start()
}
