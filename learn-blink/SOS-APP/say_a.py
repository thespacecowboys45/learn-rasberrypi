#!/usr/bin/python

print "Hello WOrld"
print("why hello world")

import RPi.GPIO as GPIO
import time

TONE_GAP=0.25 # time to wait between tones
SLEEPTIME_SHORT=0.5
SLEEPTIME_LONG=1 # 4x short value
PAUSETIME=0.5 # time to wait between letters

def clear_display(): 
    print "clear_display()"
    led_off()

def letterpause():
    print "letterpause()"
    time.sleep(PAUSETIME)

def pausetone():
    print "pausetone"
    time.sleep(TONE_GAP)

def say_a():
    print "say_a"
    morse_short()
    morse_long()
    letterpause()

def say_b():
    print "say_b"
    morse_long()
    morse_short()
    morse_short()
    morse_short()
    letterpause()

def say_c():
    print "say_c"
    morse_long()
    morse_short()
    morse_long()
    morse_short()
    letterpause()

def led_on():
    GPIO.output(18, True)

def led_off():
    GPIO.output(18, False)

def morse_short():
    print "morse_short - entry"
    led_on()
    time.sleep(SLEEPTIME_SHORT)
    led_off()
    pausetone()
    print "morse_short - exit"


def morse_long():
    print "morse_long - entry"
    led_on()
    time.sleep(SLEEPTIME_LONG)
    led_off()
    pausetone()
    print "morse_long - exit"

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

GPIO.setup(18, GPIO.OUT)

clear_display()
while True:
    say_a()
    say_b()
    say_c()
