#!/bin/bash
#
# @date 2020.07.19
# @author Space Cowboy
#
# DESCRIPTION:
#
#   Install the blink python program as a service
cp blink.py /opt
cp blink.service /etc/systemd/system/blink.service
systemctl enable blink
