import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(25, GPIO.IN)

# output a visual signal that the program is running
GPIO.output(4, True)


def handle_exception():
    print("handle_exception()")
    GPIO.output(4, False)
    GPIO.output(18, False)

def main():
    while True:
        # In this config, GPIO 25 is always on.
        # It looks like pushing the button "breaks" the connection
        if GPIO.input(25):
            print "On - False"
            GPIO.output(18, False)
        else:
            print "Off - True"
            GPIO.output(18, True)

if __name__ == "__main__":
    try:
        main()
    except:
        handle_exception()
