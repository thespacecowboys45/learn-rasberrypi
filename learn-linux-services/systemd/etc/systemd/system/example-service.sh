#!/bin/bash
#
# @date 2020.07.19
# @author The Space Cowboy
#
# DESCRIPTION:
#
# A simple loop which outputs to a file infinitely
# The intention is to run this as an example systemd
# service and watch it work.  Then extend from there.
#
##################

FILENAME="/tmp/example-service.txt"
LOOPTIME=5

while true
do
	echo "[`date`] example-service..." >> $FILENAME
	sleep $LOOPTIME
done
