#!/usr/bin/python
#
# @date 2020.07.19
# @author David Boardman
#
# DESCRIPTION:
#
#   An example service which is a python script
from datetime import datetime
import sys
import time

# Its so dumb how close python is to bash
LOOPTIME=5

filename="/tmp/davb.log"
file = open(filename, "a")
file.write("Beginning...\n")

while True:

	now=datetime.now()
	nowstr=now.strftime("%Y/%m/%d %H:%M:%S") 
	
	print("["+nowstr+"]Looping...")
	file.write("["+nowstr+"]Looping...\n")
	file.flush()
	time.sleep(LOOPTIME)

