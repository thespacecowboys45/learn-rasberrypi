EXAMPLE SYSTEMD SERVICE
-----------------------
install "service" scripts into /opt

copy example.service into /etc/systemd/system/example.service

Enable to start on boot by running:

systemctl enable example
